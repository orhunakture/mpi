#include <mpi.h>
#include <stdio.h>
#include <string.h>

#define SENDER 0
#define RECEIVER 1
#define MSG_TAG 100

int main(int argc, char** argv) {
    int world_size;
    int process_id;
    int number;
    int length;
    char *pszMsg = NULL;
    
    // Initialize
    MPI_Init(&argc, &argv);
    
    // Get the number of the processes
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    // Get the rank of the process
    MPI_Comm_rank(MPI_COMM_WORLD, &process_id);

    /* Program logic */
    if (process_id == SENDER) {
        // number = -1;
        pszMsg = "Hello from P0!";
        length = strlen(pszMsg) + 1;
        MPI_Send(pszMsg, length, MPI_BYTE, RECEIVER, MSG_TAG, MPI_COMM_WORLD);
        
    } else if (process_id == RECEIVER) {
        char buf[256];
        
        MPI_Recv(buf, 256, MPI_BYTE, SENDER, MSG_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process 1 received message \"%s\" from process 0\n", buf);
    }
    
    // Finalize the MPI
    MPI_Finalize();
    
    return 0;
}
