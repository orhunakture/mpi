#include <mpi.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MASTER      0
#define MSG_START   100
#define MSG_END     101
#define MSG_RESULT  102
#define MSG_DATA    103
#define MSG_MAX     104

#define FALSE 0
#define TRUE 1

typedef char BOOL;

#define ARR_SIZE    9

void* AllocateMemory(int size, const char* pcszErrMsg, BOOL bExit);
int* GenerateRandomIntegerArray(int size);
void PrintIntArray(int* pArr, int size);

void* AllocateMemory(int size, const char* pcszErrMsg, BOOL bExit) {
    void *pvMem;
    
    pvMem = malloc(size);
    if (NULL == pvMem) {
        fprintf(stderr, "Cannot allocate memory: %s\n", pcszErrMsg);
        if (TRUE == bExit)
            exit(1);
    }
    return pvMem;
}

int* GenerateRandomIntegerArray(int size) {
    int i;
    int *pArr;
    
    pArr = AllocateMemory(sizeof(int) *  size, "Error during GenerateRandomIntegerArray", TRUE);
    
    for (i = 0; i < size; ++i)
        pArr[i] = rand() % 100;
    
    return pArr;
}

void PrintIntArray(int* pArr, int size) {
    int i;
    
    for (i = 0; i < size; ++i)
        printf("Array[%d]: %d\n", i, pArr[i]);
}

int main(int argc, char** argv) {
    int world_size;
    int process_id;
    int start, end;
    
    srand(time(NULL));
    
    // Initialize
    MPI_Init(&argc, &argv);
    
    // Get the number of the processes
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    // Get the rank of the process
    MPI_Comm_rank(MPI_COMM_WORLD, &process_id);

    /* Program logic */
    if (process_id == MASTER) {
        int items_per_slave;
        int* pArr;
        int slave_count;
        int id;
        
        pArr = GenerateRandomIntegerArray(ARR_SIZE);
        printf("Master generated a random array. It is as follows...\n");
        PrintIntArray(pArr, ARR_SIZE);
        
        slave_count = world_size - 1;
        items_per_slave = ARR_SIZE / slave_count;
        int max = 0;
        
        for (id = MASTER + 1; id < world_size; ++id) {
            // Calculate and send start index
            start = items_per_slave * (id - 1);
            MPI_Send(&start, 1, MPI_INT, id, MSG_START, MPI_COMM_WORLD);
            
            // Calculate and send end index
            end = items_per_slave * id;
            MPI_Send(&end, 1, MPI_INT, id, MSG_END, MPI_COMM_WORLD);
            
            // Send Data
            MPI_Send(&pArr[start], items_per_slave, MPI_INT, id, MSG_DATA, MPI_COMM_WORLD);
            
            // Receive max
            MPI_Recv(&max, 1, MPI_INT, id, MSG_MAX, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            printf("Max of id:%d => %d\n", id, max);
        }
        
    } else {
        int* pData;
        int item_count;
        
        // Receive start index
        MPI_Recv(&start, 1, MPI_INT, MASTER, MSG_START, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d received START as %d\n", process_id, start);
        
        // Receive end index
        MPI_Recv(&end, 1, MPI_INT, MASTER, MSG_END, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d received END as %d\n", process_id, end);
        
        // Receive data
        item_count = end - start;
        pData = AllocateMemory(sizeof(int) * item_count, "Error during slave data memory allocation", TRUE);
        MPI_Recv(pData, item_count, MPI_INT, MASTER, MSG_DATA, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        
        printf("Slave %d received data from MASTER! They are as follows...\n", process_id);
        PrintIntArray(pData, item_count);
        
        // Find max value
        int i;
        int max = *pData;
        
        for (i = 1; i < item_count; i++) {
            if (max < *(pData + i))
                max = *(pData + i);
        }
        
        // Send max to MASTER
        MPI_Send(&max, 1, MPI_INT, MASTER, MSG_MAX, MPI_COMM_WORLD);
    }
    
    // Finalize the MPI
    MPI_Finalize();
    
    return 0;
}
